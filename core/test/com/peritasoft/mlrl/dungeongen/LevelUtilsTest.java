package com.peritasoft.mlrl.dungeongen;

import org.junit.Test;

import static org.junit.Assert.*;

public class LevelUtilsTest {

    @Test
    public void reachableArea() {
        final String[] level = {
                "#####",
                "#..>#",
                "#.#.#",
                "#.#.#",
                "#...#",
                "#.#.#",
                "#.#.#",
                "#<..#",
                "#####",
        };
        final Tile[][] tiles = stringToTiles(level);
        final Position start = new Position(3, 4);
        assertEquals(17, LevelUtils.reachableArea(new Level(tiles, start, LevelType.CAVE), start));
    }

    @Test
    public void findStaircaseInRoomNoCrashWithEmptyLevels() {
        final Tile[][] emptyLevel = {};
        assertFalse(LevelUtils.findStaircaseInRoom(emptyLevel, new Position(0, 0)));

        final Tile[][] noWidth = {new Tile[]{}};
        assertFalse(LevelUtils.findStaircaseInRoom(noWidth, new Position(0, 0)));
    }

    @Test
    public void findStaircaseInRoom() {
        final String[] level = {
                "#####",
                "#.#>#",
                "#...#",
                "#####",
                "#...#",
                "#####",
                "#.#.#",
                "#<..#",
                "#####",
        };
        final Tile[][] tiles = stringToTiles(level);
        assertTrue(LevelUtils.findStaircaseInRoom(tiles, new Position(1, 1)));
        assertFalse(LevelUtils.findStaircaseInRoom(tiles, new Position(2, 4)));
        assertFalse(LevelUtils.findStaircaseInRoom(tiles, new Position(3, 6)));
    }

    private Tile[][] stringToTiles(String[] level) {
        final int height = level.length;
        if (height == 0) throw new IllegalArgumentException("The level must have at least a row");
        final int width = level[0].length();
        if (width == 0) throw new IllegalArgumentException("The level must have at least a column");
        Tile[][] tiles = new Tile[height][width];
        for (int y = 0; y < height; y++) {
            Tile[] row = tiles[y];
            String line = level[y];
            if (line.length() != width) {
                throw new IllegalArgumentException(String.format("Row %s has a different length", y));
            }
            for (int x = 0; x < width; x++) {
                switch (line.charAt(x)) {
                    case '#':
                        row[x] = Tile.WALL;
                        break;
                    case '.':
                        row[x] = Tile.GROUND;
                        break;
                    case '<':
                        row[x] = Tile.STAIRCASEUP;
                        break;
                    case '>':
                        row[x] = Tile.STAIRCASEDOWN;
                        break;
                    default:
                        throw new IllegalArgumentException(String.format("Character %c of line %d, column %d is not valid", line.charAt(x), y, x));

                }
            }
        }
        return tiles;
    }
}
