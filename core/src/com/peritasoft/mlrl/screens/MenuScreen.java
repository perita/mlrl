/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.render.MenuRenderer;

public class MenuScreen extends MlrlScreenAdapter {
    private final MyLittleRogueLike game;
    private final MenuRenderer renderer;
    private final PlayScreen playScreen;
    private final Matrix4 oldTransformation;
    private final Matrix4 transformation;
    private final Rectangle scissors = new Rectangle();
    private float offset;
    private float alpha = 1f;

    public MenuScreen(MyLittleRogueLike game, PlayScreen playScreen) {
        if (playScreen == null) {
            throw new IllegalArgumentException("prevScreen can not be null");
        }
        oldTransformation = new Matrix4();
        transformation = new Matrix4();
        this.game = game;
        this.playScreen = playScreen;
        this.renderer = new MenuRenderer(game.getSmallFont());
        offset = this.renderer.getHeight();
    }

    @Override
    public void render(float delta) {
        playScreen.render(0f);
        if (alpha < 1f) {
            alpha -= delta * 10f;
            if (alpha < 0f) {
                returnBack();
                return;
            }
        }
        Batch batch = game.getBatch();
        offset -= delta * 1000f;
        if (offset > 0) {
            oldTransformation.set(batch.getTransformMatrix());
            transformation.idt().translate(0, -offset, 0);
            batch.setTransformMatrix(transformation);
        }
        Viewport viewport = game.getViewport();
        viewport.calculateScissors(oldTransformation, renderer.getRect(), scissors);
        if (ScissorStack.pushScissors(scissors)) {
            batch.setColor(1f, 1f, 1f, alpha);
            batch.begin();
            renderer.render(batch, playScreen.isMusicOn(), playScreen.isSfxEnabled(), playScreen.isTilesetGraphical(), playScreen.shouldShowPad(), playScreen.isPadAtRight());
            batch.end();
            batch.setColor(1f, 1f, 1f, 1f);
            ScissorStack.popScissors();
        }
        if (offset > 0) {
            batch.setTransformMatrix(oldTransformation);
        }
    }

    @Override
    public void pause() {
        playScreen.pause();
    }

    @Override
    public void resume() {
        playScreen.resume();
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.ESCAPE:
                retreat();
                return true;
            case Input.Keys.UP:
                renderer.focusPrev();
                return true;
            case Input.Keys.TAB:
                if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) {
                    renderer.focusPrev();
                } else {
                    renderer.focusNext();
                }
                return true;
            case Input.Keys.DOWN:
                renderer.focusNext();
                return true;
            case Input.Keys.ENTER:
            case Input.Keys.SPACE:
                if (renderer.buttonMusicFocused()) {
                    playScreen.toggleMusic();
                } else if (renderer.buttonSfxFocused()) {
                    playScreen.toggleSfx();
                } else if (renderer.buttonTilesetFocused()) {
                    playScreen.toggleTileset();
                    return true;
                } else if (renderer.buttonPadFocused()) {
                    playScreen.togglePad();
                    return true;
                } else if (renderer.buttonPadPosFocused()) {
                    playScreen.togglePadPos();
                    return true;
                } else if (renderer.buttonAbandonFocused()) {
                    abandonQuest();
                    return true;
                }
                return true;
        }
        return false;
    }

    private void retreat() {
        alpha = 0.9f;
    }

    private void returnBack() {
        game.setScreen(playScreen);
        dispose();
    }

    @Override
    public boolean touchDown(Vector2 touchPoint, int pointer, int button) {
        if (button != Input.Buttons.LEFT || pointer > 0) return false;
        if (renderer.buttonMusicPressed(touchPoint)) {
            playScreen.toggleMusic();
            return true;
        } else if (renderer.buttonSfxPressed(touchPoint)) {
            playScreen.toggleSfx();
            return true;
        } else if (renderer.buttonTilesetPressed(touchPoint)) {
            playScreen.toggleTileset();
            return true;
        } else if (renderer.buttonPadPressed(touchPoint)) {
            playScreen.togglePad();
            return true;
        } else if (renderer.buttonPadPosPressed(touchPoint)) {
            playScreen.togglePadPos();
            return true;
        } else if (renderer.buttonAbandonPressed(touchPoint)) {
            abandonQuest();
            return true;
        } else if (playScreen.buttonConfigPressed(touchPoint)) {
            retreat();
        }
        return false;
    }

    private void abandonQuest() {
        this.dispose();
        playScreen.abandonQuest();
    }

    @Override
    public void dispose() {
        renderer.dispose();
    }
}
