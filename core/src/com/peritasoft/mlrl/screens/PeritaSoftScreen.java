/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.peritasoft.mlrl.MyLittleRogueLike;

public class PeritaSoftScreen extends MlrlScreenAdapter {
    private static final float TIMER = 3f;
    private static final int TILE_SIZE = 64;
    private final BitmapFont font;
    private final Texture sheet;
    private MyLittleRogueLike game;
    private float screenTime;
    private float titleTime;
    private float stateTime;
    private Animation<TextureRegion> animatedLogo;

    public PeritaSoftScreen(MyLittleRogueLike game) {
        this.game = game;
        screenTime = 0f;
        titleTime = 0f;
        stateTime = 0f;
        sheet = new Texture("PeritaAnimated.png");
        animatedLogo = new Animation<>(0.1f, split(sheet));
        font = game.getTextFont();
    }

    @Override
    public void render(float delta) {
        update(delta);
        draw();
    }

    private void update(float delta) {
        if (screenTime < TIMER) {
            screenTime += delta;
            titleTime += delta;
            if (titleTime / 2 >= 1) stateTime += delta;
        } else {
            nextScreen();
        }
    }

    private void nextScreen() {
        game.setScreen(new TitleScreen(game));
        dispose();
    }

    private void draw() {
        float alpha = titleTime / 2f;
        TextureRegion region = animatedLogo.getKeyFrame(stateTime, false);

        game.getBatch().begin();
        game.getBatch().draw(region, (MyLittleRogueLike.SCREENWIDTH / 2f) - (TILE_SIZE / 2f), (MyLittleRogueLike.SCREENHEIGHT / 2f) + 25f,
                TILE_SIZE / 2f, TILE_SIZE / 2f,
                TILE_SIZE, TILE_SIZE,
                1.5f, 1.5f,
                0f);

        font.getData().setScale(1.5f);
        font.setColor(new Color(0.6f, 0.9f, 0.31f, alpha));
        font.draw(game.getBatch(), "Perita Soft",
                0f, MyLittleRogueLike.SCREENHEIGHT / 2f,
                MyLittleRogueLike.SCREENWIDTH, 1, true);
        font.getData().setScale(1f);

        game.getBatch().end();
    }

    private TextureRegion[] split(Texture sheet) {
        TextureRegion[] regions = new TextureRegion[7];
        for (int t = 0; t < 6; t++) {
            regions[t] = (new TextureRegion(sheet, TILE_SIZE * t, 0, TILE_SIZE, TILE_SIZE));
        }
        regions[6] = regions[0];
        return regions;
    }

    @Override
    public boolean keyDown(int keycode) {
        nextScreen();
        return true;
    }

    @Override
    public boolean touchDown(Vector2 touchPoint, int pointer, int button) {
        nextScreen();
        return true;
    }

    @Override
    public void dispose() {
        sheet.dispose();
    }
}
