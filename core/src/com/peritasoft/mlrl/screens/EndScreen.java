/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.events.MusicManager;
import com.peritasoft.mlrl.events.SfxManager;

public class EndScreen extends MlrlScreenAdapter {
    private static final float TIMER = 6f;
    private final MyLittleRogueLike game;
    private float screenTime;
    private final Texture img;
    private final MusicManager music;
    private final SfxManager sfx;

    public EndScreen(MyLittleRogueLike game, MusicManager music, SfxManager sfx) {
        this.game = game;
        screenTime = 0f;
        img = new Texture("end.png");
        this.music = music;
        this.sfx = sfx;
    }

    @Override
    public void render(float delta) {
        update(delta);
        draw();
    }

    private void update(float delta) {
        if (screenTime < TIMER) {
            screenTime += delta;
        } else {
            nextScreen();
        }
    }

    private void nextScreen() {
        game.setScreen(new TitleScreen(game));
        dispose();
    }

    private void draw() {
        float alpha = screenTime / 2f;
        game.getBatch().begin();
        game.getBatch().setColor(1,1,1, alpha);
        game.getBatch().draw(img, 0, 0);
        game.getBatch().setColor(1,1,1, 1);
        game.getBatch().end();
    }


    @Override
    public boolean keyDown(int keycode) {
        nextScreen();
        return true;
    }

    @Override
    public boolean touchDown(Vector2 touchPoint, int pointer, int button) {
        nextScreen();
        return true;
    }

    @Override
    public void dispose() {
        img.dispose();
        music.dispose();
        sfx.dispose();
    }
}
