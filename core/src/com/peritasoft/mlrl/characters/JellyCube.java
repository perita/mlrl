/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.ai.WanderSeekApproach;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Inventory;
import com.peritasoft.mlrl.item.Item;

public class JellyCube extends Character {
    public JellyCube(int level, Position pos) {
        this(level, pos, new Inventory());
    }

    public JellyCube(int level, Position pos, Inventory inventory) {
        super(Demography.JELLY_CUBE, level, 0, 0, 0, 0, 4, pos.getX(), pos.getY(), new WanderSeekApproach(), inventory);
        this.setStr(level);
        this.setDex((level / 4) + 1);
        this.setWis(1);
        this.setCon((level * 2) - (level / 4));
        this.resetHp();
    }

    @Override
    public void attack(Character obj, Level level) {
        super.attack(obj, level);
        if (MathUtils.randomBoolean(0.35f)) {
            Item stealedWeapon = obj.giveEquipedWeapon();
            if (stealedWeapon != null) {
                getInventory().add(stealedWeapon);
                GameEvent.weaponEmbedded(this);
            }
        }
    }

    @Override
    public boolean onKilled(Level level, Character killer) {
        dropItems(level, 1f);
        return super.onKilled(level, killer);
    }
}
