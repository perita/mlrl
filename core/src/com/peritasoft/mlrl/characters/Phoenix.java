/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.characters;

import com.peritasoft.mlrl.ai.ApproachAndAttack;
import com.peritasoft.mlrl.ai.Behaviour;
import com.peritasoft.mlrl.ai.WeaponWielder;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.Rests;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.item.Inventory;

public class Phoenix extends Character {

    public Phoenix(Demography demography, int level, Position pos, Behaviour ai, Inventory inventory) {
        super(demography, level, 0, 0, 0, 0, 6, pos.getX(), pos.getY(), new WeaponWielder(ai), inventory);
        this.givesXP = false;
    }

    public Phoenix(int level, Position pos) {
        this(level, pos, new Inventory());
    }

    public Phoenix(int level, Position pos, Inventory inventory) {
        this(Demography.PHOENIX, level, pos, new ApproachAndAttack(), inventory);
        this.setStr(level);
        this.setDex(level);
        this.setWis(level);
        this.setCon((int) (level * 1.5f));
        this.resetHp();
    }


    @Override
    public boolean onKilled(Level level, Character killer) {
        dropItems(level);
        level.add(new Rests(getPosition(), 10, this, level));
        return true;
    }

    @Override
    public boolean raise(Level level) {
        resetHp();
        if (level.hasCharacterIn(getPosition())) return false;
        level.addEnemy(this);
        if (level.getCell(getPosition()).hasItem()) {
            getInventory().add(level.getCell(getPosition()).pickupItem(this));
        }
        GameEvent.raised(this);
        return true;
    }

    @Override
    public boolean isFlamable() {
        return false;
    }
}
