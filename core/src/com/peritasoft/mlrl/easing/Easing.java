/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.easing;

import com.badlogic.gdx.math.MathUtils;

public abstract class Easing {
    private float stateTime;
    private final float delay;
    private final float from;
    private final float to;
    private final float limit = 0.75f;

    public Easing(float from, float to, float delay) {
        this.from = from;
        this.to = to;
        this.delay = delay;
    }

    public void update(float delta) {
        stateTime += delta;
    }

    public float getValue() {
        if (stateTime < delay) {
            return from;
        }
        final float time = stateTime - delay;
        if (time > limit) {
            return to;
        }
        return MathUtils.lerp(from, to, ease(time / limit));
    }

    public boolean isDone() {
        return (stateTime - delay) > limit;
    }

    protected abstract float ease(float t);
}


