/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Toggle implements Widget {
    private final Rectangle rect;
    private final TextureRegion on;
    private final TextureRegion off;

    public Toggle(float x, float y, TextureRegion on, TextureRegion off) {
        this(x, y, on.getRegionWidth(), on.getRegionHeight(), on, off);
    }

    public Toggle(float x, float y, float width, float height, TextureRegion on, TextureRegion off) {
        this.rect = new Rectangle(x, y, width, height);
        this.on = on;
        this.off = off;
    }

    public void draw(Batch batch, boolean isOn) {
        batch.draw(isOn ? on : off, rect.x, rect.y, rect.width, rect.height);
    }

    @Override
    public Rectangle getRect() {
        return rect;
    }

    public float getY() {
        return rect.y;
    }

    public float getX() {
        return rect.x;
    }

    public float getCenterX() {
        return getX() + (rect.width/2);
    }

    public float getCenterY() {
        return getY() + (rect.height/2);
    }

    public boolean pressed(Vector2 position) {
        return rect.contains(position);
    }
}
