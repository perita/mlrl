/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.render.TextureAtlasHelper;

public class PanelAscii implements Disposable {

    private final BitmapFont font;
    private final TextureAtlas atlas;
    private final TextureAtlas.AtlasRegion topLeft;
    private final TextureAtlas.AtlasRegion topRight;
    private final TextureAtlas.AtlasRegion bottomRight;
    private final TextureAtlas.AtlasRegion bottomLeft;
    private final TextureAtlas.AtlasRegion vertical;
    private final TextureAtlas.AtlasRegion horizontal;
    private final TextureAtlas.AtlasRegion background;

    public PanelAscii(BitmapFont font) {
        this.font = font;
        atlas = new TextureAtlas("terminal.atlas");
        topLeft = TextureAtlasHelper.mustFindRegion(atlas, "┌");
        topRight = TextureAtlasHelper.mustFindRegion(atlas, "┐");
        bottomRight = TextureAtlasHelper.mustFindRegion(atlas, "┘");
        bottomLeft = TextureAtlasHelper.mustFindRegion(atlas, "└");
        vertical = TextureAtlasHelper.mustFindRegion(atlas, "│");
        horizontal = TextureAtlasHelper.mustFindRegion(atlas, "─");
        background = TextureAtlasHelper.mustFindRegion(atlas, "(space)");
    }

    @Override
    public void dispose() {
        atlas.dispose();
    }

    public void draw(SpriteBatch batch, Rectangle rect, String label) {
        final int glyphSize = 16;

        final float ch = rect.getHeight() / glyphSize;
        final float cw = rect.getWidth() / glyphSize;

        final float leftCol = (rect.getX() / glyphSize) * glyphSize;
        final float rightCol = leftCol + cw * glyphSize;
        final float bottomRow = (rect.getY() / glyphSize) * glyphSize;
        final float topRow = bottomRow + ch * glyphSize;

        batch.draw(bottomLeft, leftCol, bottomRow);
        batch.draw(topLeft, leftCol, topRow);
        batch.draw(bottomRight, rightCol, bottomRow);
        batch.draw(topRight, rightCol, topRow);

        for (int cx = 1; cx < cw; cx++) {
            final float x = leftCol + cx * glyphSize;
            batch.draw(horizontal, x, bottomRow);
            batch.draw(horizontal, x, topRow);
            for (int cy = 1; cy < ch; cy++) {
                batch.draw(background, x, rect.getY() + cy * glyphSize);
            }
        }
        for (int cy = 1; cy < ch; cy++) {
            final float y = rect.getY() + cy * glyphSize;
            batch.draw(vertical, leftCol, y);
            batch.draw(vertical, rightCol, y);
        }

        font.draw(batch, label, leftCol + 8, bottomRow + (topRow - bottomRow) / 2 + 12,
                rightCol - leftCol, Align.center, true);
    }
}
