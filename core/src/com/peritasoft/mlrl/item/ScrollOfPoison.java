/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.effects.Poison;
import com.peritasoft.mlrl.events.GameEvent;

public class ScrollOfPoison extends Item implements Scroll {
    private final int level;

    public ScrollOfPoison(int level) {
        super("Scroll of Poison", "This spell scroll bears the words of a single poison cloud spell, written in a mystical cipher.");
        this.level = level + 1;
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.SCROLL_POISON;
    }

    @Override
    public boolean isEquipable() {
        return false;
    }

    @Override
    public boolean isUsable() {
        return true;
    }

    @Override
    public void use(Character character, Level level) {
        GameEvent.poison();
        for (int y = -1; y <= 1; y++) {
            for (int x = -1; x <= 1; x++) {
                if (y == 0 && x == 0) continue;
                level.getLifeObjs().add(new Poison(this.level, level, character, x, y));
            }
        }
    }

    public int getLevel() {
        return level - 1;
    }
}
