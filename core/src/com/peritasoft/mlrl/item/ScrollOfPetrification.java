/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.item;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.ProjectileType;
import com.peritasoft.mlrl.events.GameEvent;
import com.peritasoft.mlrl.weapons.Ammo;
import com.peritasoft.mlrl.weapons.Shootable;

public class ScrollOfPetrification extends Item implements Shootable, Scroll {
    private final int level;

    public ScrollOfPetrification(int level) {
        super("Scroll of Paralyze", "This spell scroll bears the words of a single paralyze spell, written in a mystical cipher.");
        this.level = level + 1;
    }

    @Override
    public ItemCategory getCategory() {
        return ItemCategory.SCROLL_PETRIFY;
    }

    @Override
    public boolean isEquipable() {
        return false;
    }

    @Override
    public boolean isUsable() {
        return true;
    }

    @Override
    public void use(Character character, Level level) {
        character.aim(this, level);
    }

    @Override
    public int getRange() {
        return Math.min(level, 6);
    }

    @Override
    public boolean canShoot(Character shooter) {
        return true;
    }

    @Override
    public ProjectileType shoot(Character shooter, Position target, Level level) {
        shooter.getInventory().remove(this);
        return ProjectileType.VOIDBALL;
    }

    @Override
    public void miss(Character shooter, Level level, Position pos) {
        GameEvent.shootMissed();
    }

    @Override
    public void impact(Character shooter, Character target, Level level) {
        int turns = target.petrify();
        GameEvent.petrified(target, turns);
    }

    @Override
    public Ammo getAmmo() {
        return null;
    }

    public int getLevel() {
        return level - 1;
    }
}
