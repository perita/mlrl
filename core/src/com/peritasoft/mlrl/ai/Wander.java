/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;

import java.util.Queue;

public class Wander extends Behaviour {
    private Queue<Position> path;
    private final Position lastPosition = new Position(-1, -1);

    @Override
    public Action update(final Level level, Character me, Character enemy) {
        while (path == null || path.isEmpty() || !me.getPosition().equals(lastPosition)) {
            lastPosition.set(me.getPosition());
            final Position pathGoal = findValidPosition(level, me);
            path = PathFinder.find(level, me.getPosition(), pathGoal, new PathFinder.Traversor() {
                @Override
                public boolean isTraverseable(int x, int y) {
                    return level.isWalkable(x, y);
                }
            });
        }
        Position pos = path.remove();
        Direction dir = me.getPosition().directionTo(pos);
        Action action = me.move(dir, level);
        lastPosition.set(me.getPosition());
        if (action == Action.COLLIDE) {
            path.clear();
        }
        return action;
    }

    private static Position findValidPosition(Level level, Character me) {
        final Position pos = new Position(me.getPosition());
        final int minX = pos.getX() - me.getSightRadius() - 1;
        final int maxX = pos.getX() + me.getSightRadius() + 1;
        final int minY = pos.getY() - me.getSightRadius() - 1;
        final int maxY = pos.getY() + me.getSightRadius() + 1;
        while (pos.equals(me.getPosition()) || !level.isWalkable(pos)) {
            pos.set(
                    MathUtils.random(minX, maxX),
                    MathUtils.random(minY, maxY)
            );
        }
        return pos;
    }
}
