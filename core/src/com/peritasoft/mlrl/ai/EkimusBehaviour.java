/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.characters.Demography;
import com.peritasoft.mlrl.characters.Ekimus;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.weapons.Teleport;

public class EkimusBehaviour extends Behaviour {
    private static final int MP_TURNS = 4;
    private final Behaviour vampireForm;
    private final Behaviour batForm;
    private int turnCounter;
    private boolean blinked;
    private final Teleport teleportSpell;

    public EkimusBehaviour() {
        vampireForm = new Charge(new ApproachAndAttack(),3);
        batForm = new Runaway();
        turnCounter = 0;
        blinked = false;
        teleportSpell = new Teleport();
    }

    @Override
    public Action update(Level level, Character me, Character enemy) {
        turnCounter++;
        switch (me.getDemography()) {
            case EKIMUS:
                if (MathUtils.randomBoolean() && isLowHp(me)) {
                    return switchTo(Demography.MIST, me);
                }
                if (me.getMp() < me.getMaxMp() / 3 ) {
                    return switchTo(Demography.BAT_GIANT, me);
                }
                return vampireForm.update(level, me, enemy);
            case BAT_GIANT:
                if (MathUtils.randomBoolean() && isLowHp(me)) {
                    return switchTo(Demography.MIST, me);
                }
                if (turnCounter >= MP_TURNS) {
                    me.fullMp();
                    return switchTo(Demography.EKIMUS, me);
                }
                return batForm.update(level, me, enemy);
            case MIST:
                if (blinked) {
                    blinked = false;
                    int targetHp = (int) (me.getMaxHp() / 5 + (MathUtils.random(0.05f) * me.getMaxHp()));
                    me.heal(targetHp - me.getHp());
                    return switchTo(Demography.EKIMUS, me);
                } else {
                    blinked = true;
                    teleportSpell.cast(me, level);
                    return Action.USE_ITEM;
                }
            default:
                throw new IllegalStateException();
        }
    }

    private boolean isLowHp(Character me) {
        return me.getHp() < me.getMaxHp() / 5;
    }

    private Action switchTo(Demography demography, Character me) {
        turnCounter = 0;
        dexByDemography(me, demography);
        ((Ekimus) me).setDemography(demography);
        return Action.SHAPE_SHIFT;
    }

    private void dexByDemography(Character me, Demography targetDemography) {
        int factor = 8;
        if (me.getDemography() == Demography.BAT_GIANT) {
            me.setDex(me.getDex() - factor);
        }
        if (targetDemography == Demography.BAT_GIANT) {
            me.setDex(me.getDex() + factor);
        }
    }
}
