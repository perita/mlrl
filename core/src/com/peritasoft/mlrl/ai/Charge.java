/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.ai;

import com.peritasoft.mlrl.characters.Action;
import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.effects.Crosshairs;

public class Charge extends Behaviour {
    private final Behaviour next;
    private final int coolOff;
    private int coolOffTurn;

    public Charge(Behaviour next, int coolOff) {
        this.next = next;
        this.coolOff = coolOff;
        this.coolOffTurn = 0;
    }

    @Override
    public Action update(final Level level, final Character me, final Character enemy) {
        coolOffTurn += 1;
        if (enemy == null) {
            return next.update(level, me, enemy);
        }
        if (coolOffTurn >= coolOff) {
            Position enemyPos = enemy.getPrevPosition();
            int distance = me.getPosition().distance(enemyPos);
            if (me.canFireWeapon() && me.getFireRange() >= distance
                    && level.hasNoObstacles(me.getPosition(), enemyPos)) {
                coolOffTurn = 0;
                Crosshairs xh = new Crosshairs(enemyPos, Direction.NONE, me.shootWeapon());
                me.shoot(level, xh);
                return Action.ATTACK;
            }
        }
        return next.update(level, me, enemy);
    }

}
