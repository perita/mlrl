/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.serialization;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.peritasoft.mlrl.weapons.Grimoire;

abstract class GrimoireSerializer<T extends Grimoire> implements Json.Serializer<T> {
    @Override
    public void write(Json json, T object, Class knownType) {
        json.writeArrayStart();
        json.writeValue(object.getMinWis());
        json.writeValue(object.getManaCost());
        json.writeValue(object.getMinDistance());
        json.writeValue(object.getBonusDamage());
        json.writeArrayEnd();
    }

    @Override
    public T read(Json json, JsonValue jsonData, Class type) {
        final int[] stats = jsonData.asIntArray();
        if (stats.length != 4) {
            throw new IllegalArgumentException("Incorrect stats for grimoire");
        }
        final int minWis = stats[0];
        final int manaCost = stats[1];
        final int minDistance = stats[2];
        final int bonusDamage = stats[3];
        return createGrimoire(minWis, manaCost, minDistance, bonusDamage);
    }

    protected abstract T createGrimoire(int minWis, int manaCost, int minDistance, int bonusDamage);
}
