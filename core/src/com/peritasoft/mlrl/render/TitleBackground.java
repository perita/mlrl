/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ScreenUtils;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.easing.EaseInBack;
import com.peritasoft.mlrl.easing.EaseOutBack;
import com.peritasoft.mlrl.easing.Easing;

public class TitleBackground implements Disposable {

    private final TextureAtlas atlas;
    private final TextureRegion background;
    private final Animation<TextureRegion> torch;
    private final Animation<TextureRegion> death;
    private TextureRegion title;
    private final TextureRegion mlrl;
    private final TextureRegion select;
    private float stateTime;
    private final Vector2 titlePos;
    private Easing titleEasing;
    private final Music music;

    public TitleBackground() {
        atlas = new TextureAtlas("title.atlas");
        background = TextureAtlasHelper.mustFindRegion(atlas, "background");
        torch = new Animation<TextureRegion>(0.2f, TextureAtlasHelper.mustFindRegions(atlas, "torch", 1, 10));
        death = new Animation<TextureRegion>(0.15f, TextureAtlasHelper.mustFindRegions(atlas, "death", 1, 18));
        mlrl = TextureAtlasHelper.mustFindRegion(atlas, "mlrl");
        select = TextureAtlasHelper.mustFindRegion(atlas, "select");
        titlePos = new Vector2();

        music = Gdx.audio.newMusic(Gdx.files.internal("music/menu.mp3"));
        music.setLooping(true);
        music.setVolume(0.35f);
        music.play();
    }

    @Override
    public void dispose() {
        music.dispose();
        atlas.dispose();
    }

    public void update(float delta) {
        stateTime += delta;
        titleEasing.update(delta);
    }

    public void draw(Batch batch) {
        final float fadeTime = 0.5f;
        if (stateTime < fadeTime) {
            final float alpha = MathUtils.lerp(0f, 1f, stateTime / fadeTime);
            batch.setColor(1f, 1f, 1f, alpha);
        }
        batch.draw(background, 0f, 0f);
        batch.draw(torch.getKeyFrame(stateTime, true), 352f, 80f);
        final float deathStill = 3.5f;
        batch.draw(death.getKeyFrame(stateTime > deathStill ? stateTime - deathStill : 0, false), 198f, 113f);
        batch.draw(title, titlePos.x, titleEasing.getValue());

        batch.setColor(1f, 1f, 1f, 1f);
    }

    public void hideTitle() {
        titleEasing = new EaseInBack(titlePos.y, MyLittleRogueLike.SCREENHEIGHT + mlrl.getRegionHeight(), 0f);
    }

    public void clear() {
        ScreenUtils.clear(2f / 255f, 16f / 255f, 19f / 255f, 1f);
    }

    public void setMlrlTitle() {
        setTitle(mlrl);
    }

    public void setSelectTitle() {
        setTitle(select);
    }

    private void setTitle(TextureRegion titleRegion) {
        title = titleRegion;
        titlePos.set(MyLittleRogueLike.SCREENWIDTH / 2f - title.getRegionWidth() / 2f, MyLittleRogueLike.SCREENHEIGHT - title.getRegionHeight());
        titleEasing = new EaseOutBack(MyLittleRogueLike.SCREENHEIGHT + title.getRegionHeight(), titlePos.y, 0f);
    }

}
