/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class PrettyRendererAtlas extends RendererAtlas {
    private final TextureAtlas world;
    private final TextureAtlas monsters;
    private final TextureAtlas items;
    private final TextureAtlas fx;
    private final TextureAtlas statusFx;
    private final TextureFloatingNumberRenderer floatingNumberRenderer;

    public PrettyRendererAtlas() {
        world = new TextureAtlas("td_world.atlas");
        monsters = new TextureAtlas("td_monsters.atlas");
        items = new TextureAtlas("td_items.atlas");
        fx = new TextureAtlas("td_fx.atlas");
        statusFx = new TextureAtlas("statusFx.atlas");
        loadCharacters(monsters);
        loadTerrains(world);
        loadObjects(world);
        loadItems(items);
        loadProps(world);
        loadFx(fx);
        loadStatusFx(statusFx);
        floatingNumberRenderer = new TextureFloatingNumberRenderer();
    }

    @Override
    public void dispose() {
        items.dispose();
        monsters.dispose();
        world.dispose();
        fx.dispose();
        statusFx.dispose();
    }

    @Override
    public FloatingNumberRenderer getFloatingNumberRenderer() {
        return floatingNumberRenderer;
    }
}
