/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.StreamUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class FontTextureAtlas extends TextureAtlas {

    private final Map<String, GlyphColor> charmap = new HashMap<>();

    public FontTextureAtlas(String internalPackFile, String internalCharmapFile) {
        super(internalPackFile);

        FileHandle charmapFile = Gdx.files.internal(internalCharmapFile);
        BufferedReader reader = new BufferedReader(new InputStreamReader(charmapFile.read()), 64);
        try {
            while (true) {
                String line = reader.readLine();
                if (line == null) break;
                if (line.trim().length() == 0) continue;
                String[] fields = line.split("\\s+");
                if (fields.length != 3) continue;
                charmap.put(fields[0], new GlyphColor(fields[1], Color.valueOf(fields[2])));
            }
        } catch (Exception ex) {
            throw new GdxRuntimeException("Error reading charmap file: " + charmapFile, ex);
        } finally {
            StreamUtils.closeQuietly(reader);
        }
    }

    @Override
    public AtlasRegion findRegion(String name, int index) {
        // Ignore index
        return findRegion(name);
    }

    @Override
    public AtlasRegion findRegion(String name) {
        GlyphColor original = charmap.get(name);
        AtlasRegion region = super.findRegion(original.glyph);
        if (region == null) {
            throw new IllegalArgumentException("Could not find character '" + original.glyph + "' for atlas name '" + name + "'");
        }
        return region;
    }

    @Override
    public Sprite createSprite(String name, int index) {
        // Ignore index
        return createSprite(name);
    }

    @Override
    public Sprite createSprite(String name) {
        super.createSprite(name);
        GlyphColor original = charmap.get(name);
        Sprite sprite = super.createSprite(original.glyph);
        if (sprite == null) {
            throw new IllegalArgumentException("Could not find character '" + original.glyph + "' for atlas name '" + name + "'");
        }
        sprite.setColor(original.color);
        return sprite;
    }

    private static class GlyphColor {
        public final String glyph;
        public final Color color;

        private GlyphColor(String glyph, Color color) {
            this.glyph = glyph;
            this.color = color;
        }
    }
}
