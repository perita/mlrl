/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.ui.Toggle;

public class Pad {
    private final Toggle[] directions;
    private final Toggle shoot;
    private final Toggle skip;
    private final Toggle aim;
    public final int x;
    public final int y;
    private Direction pressedDir;
    private boolean centerPressed;
    private boolean aimPressed;

    public Pad(int x, int y, TextureAtlas atlas, Direction pos) {
        this.x = x;
        this.y = y;
        pressedDir = Direction.NONE;

        directions = new Toggle[Direction.values().length];
        directions[Direction.NORTH.ordinal()] = createDirection(atlas, x, y, Direction.NORTH);
        directions[Direction.WEST.ordinal()] = createDirection(atlas, x, y, Direction.WEST);
        directions[Direction.SOUTH.ordinal()] = createDirection(atlas, x, y, Direction.SOUTH);
        directions[Direction.EAST.ordinal()] = createDirection(atlas, x, y, Direction.EAST);
        skip = createCenterButton(atlas, x, y, "padSkip");
        shoot = createCenterButton(atlas, x, y, "padFire");
        aim = createAimButton(atlas, x, y, pos);
    }

    private static Toggle createDirection(TextureAtlas atlas, int x, int y, Direction dir) {
        final TextureAtlas.AtlasRegion padOn = atlas.findRegion("padOn", dir.ordinal());
        final TextureAtlas.AtlasRegion padOff = atlas.findRegion("padOff", dir.ordinal());
        final int width = padOn.originalWidth;
        final int height = padOn.originalHeight;
        final int margin = 3;
        return new Toggle(x - width / 2f + dir.x * (width + margin),
                y - height / 2f + dir.y * (height + margin),
                width, height,
                padOn, padOff);
    }

    private static Toggle createCenterButton(TextureAtlas atlas, int x, int y, String region) {
        final TextureAtlas.AtlasRegion on = atlas.findRegion(region, 0);
        final TextureAtlas.AtlasRegion off = atlas.findRegion(region, 1);
        final int width = on.originalWidth;
        final int height = on.originalHeight;
        return new Toggle(x - width / 2f, y - height / 2f, width, height, on, off);
    }

    private static Toggle createAimButton(TextureAtlas atlas, int x, int y, Direction pos) {
        final TextureAtlas.AtlasRegion on = atlas.findRegion("padAim", 0);
        final TextureAtlas.AtlasRegion off = atlas.findRegion("padAim", 1);
        final int width = on.originalWidth;
        final int height = on.originalHeight;
        final int margin = 8;
        return new Toggle(x - width / 2f + pos.x * (width + margin),
                y - height / 2f + (height + margin),
                width, height,
                on, off);
    }

    public void render(Batch batch, boolean isAiming) {
        for (Direction dir : Direction.values()) {
            if (dir == Direction.NONE) continue;
            directions[dir.ordinal()].draw(batch, pressedDir == dir);
        }
        if (isAiming) {
            shoot.draw(batch, centerPressed);
        } else {
            skip.draw(batch, centerPressed);
        }
        aim.draw(batch, aimPressed);
    }

    public Direction pressedDirection(Vector2 position) {
        pressedDir = Direction.NONE;
        for (Direction dir : Direction.values()) {
            if (dir == Direction.NONE) continue;
            if (directions[dir.ordinal()].pressed(position)) {
                pressedDir = dir;
                break;
            }
        }
        return pressedDir;
    }

    public boolean reset() {
        centerPressed = false;
        aimPressed = false;
        if (pressedDir == Direction.NONE) return false;
        pressedDir = Direction.NONE;
        return true;
    }

    public boolean isCenterPressed(Vector2 position) {
        centerPressed = skip.pressed(position);
        return centerPressed;
    }

    public boolean isAimPressed(Vector2 position) {
        aimPressed = aim.pressed(position);
        return aimPressed;
    }
}
