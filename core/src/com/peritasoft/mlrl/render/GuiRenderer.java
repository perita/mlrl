/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.mlrl.MyLittleRogueLike;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.dungeongen.Direction;
import com.peritasoft.mlrl.dungeongen.LevelType;
import com.peritasoft.mlrl.dungeongen.Tile;
import com.peritasoft.mlrl.dungeongen.WallJoint;
import com.peritasoft.mlrl.ui.Panel;
import com.peritasoft.mlrl.ui.PanelAscii;
import com.peritasoft.mlrl.ui.Toggle;
import com.peritasoft.mlrl.weapons.Ammo;
import com.peritasoft.mlrl.weapons.Shootable;


public class GuiRenderer implements Disposable {
    public static final int BOTTOM_LINE = 2;
    private final BitmapFont textFont;
    private final SpriteBatch batch;
    private final NinePatch listBackground;
    private final TextureAtlas interfaceAtlas;
    private final NinePatch guiBackground;
    private final NinePatch scrollBackground;
    private final TextureRegion emptyBar;
    private final TextureRegion hpBar;
    private final TextureRegion manaBar;
    //buttons
    private final Toggle buttonInventory;
    private final Toggle buttonConfig;
    private final Toggle buttonAim;
    private final Toggle buttonSkip;
    private final Pad[] pads;
    private int currentPad = 0;
    private final Panel deadPanel;
    private final PanelAscii asciiPanel;
    private final RendererAtlas atlas = new PrettyRendererAtlas();

    private boolean useGraphics;

    private static final int GLYPH_WIDTH = 15;
    private static final int GLYPH_HEIGHT = 15;

    public GuiRenderer(SpriteBatch batch, BitmapFont textFont) {
        this.textFont = textFont;
        this.batch = batch;
        useGraphics = true;
        interfaceAtlas = new TextureAtlas(Gdx.files.internal("td_interface.atlas"));
        buttonAim = createToggle(4f, "buttonAim");
        buttonSkip = createToggle(40f, "buttonSkip");
        buttonInventory = createToggle(MyLittleRogueLike.SCREENWIDTH - 68f, "buttonInventory");
        buttonConfig = createToggle(MyLittleRogueLike.SCREENWIDTH - 32f, "buttonConfig");
        guiBackground = new NinePatch(interfaceAtlas.findRegion("backgroundGui", 1), 16, 16, 16, 16);
        scrollBackground = new NinePatch(interfaceAtlas.findRegion("backgroundInventory"), 16, 16, 16, 16);
        listBackground = new NinePatch(interfaceAtlas.findRegion("backgroundList"), 10, 10, 5, 5);
        emptyBar = interfaceAtlas.findRegion("emptyBar");
        hpBar = interfaceAtlas.findRegion("hpBar");
        manaBar = interfaceAtlas.findRegion("manaBar");
        deadPanel = new Panel(MyLittleRogueLike.SCREENWIDTH / 2, MyLittleRogueLike.SCREENHEIGHT / 2,
                interfaceAtlas.findRegion("deadPanel"));
        asciiPanel = new PanelAscii(textFont);
        pads = new Pad[]{
                new Pad(MyLittleRogueLike.SCREENWIDTH - 70, (MyLittleRogueLike.SCREENHEIGHT / 2) - 20, interfaceAtlas, Direction.EAST),
                new Pad(70, (MyLittleRogueLike.SCREENHEIGHT / 2) - 20, interfaceAtlas, Direction.WEST)
        };
    }

    private Toggle createToggle(float x, String name) {
        final TextureRegion off = interfaceAtlas.findRegion(name, 1);
        final TextureRegion on = interfaceAtlas.findRegion(name, 2);
        final float width = (on.getRegionWidth() * 2) - 4;
        final int height = (on.getRegionHeight() * 2) - 4;
        return new Toggle(x, 2f, width, height, on, off);
    }

    public void render(int floor, PlayerHero player, boolean isAiming, boolean isDead, boolean isSkipping, boolean inInventory, boolean inSettings, boolean showPad, boolean isPadAtRight) {
        guiBackground.draw(batch, 0, 0, MyLittleRogueLike.SCREENWIDTH, 32);

        buttonAim.draw(batch, isAiming);
        buttonSkip.draw(batch, isSkipping);
        buttonInventory.draw(batch, inInventory);
        buttonConfig.draw(batch, inSettings);

        //HP Bar
        float filledWHP = ((float) Math.max(player.getHp(), 0) / player.getMaxHp()) * hpBar.getRegionWidth();
        batch.draw(emptyBar, GLYPH_WIDTH * 5, BOTTOM_LINE - 2f, 0, 0, emptyBar.getRegionWidth(), emptyBar.getRegionHeight(), 2f, 2f, 0f);
        batch.draw(hpBar.getTexture(), GLYPH_WIDTH * 5, BOTTOM_LINE - 2f, 0, 0, filledWHP, hpBar.getRegionHeight(), 2f, 2f, 0f,
                hpBar.getRegionX(), hpBar.getRegionY(), (int) filledWHP, hpBar.getRegionHeight(), false, false);

        //Mana Bar
        float filledWMana = ((float) Math.max(player.getMp(), 0) / player.getMaxMp()) * manaBar.getRegionWidth();
        batch.draw(emptyBar, GLYPH_WIDTH * 12, BOTTOM_LINE - 2f, 0, 0, emptyBar.getRegionWidth(), emptyBar.getRegionHeight(), 2f, 2f, 0f);
        batch.draw(manaBar.getTexture(), GLYPH_WIDTH * 12, BOTTOM_LINE - 2f, 0, 0, filledWMana, manaBar.getRegionHeight(), 2f, 2f, 0f,
                manaBar.getRegionX(), manaBar.getRegionY(), (int) filledWMana, manaBar.getRegionHeight(), false, false);

        Shootable w = player.shootWeapon();
        Ammo ammo = w == null ? null : w.getAmmo();
        listBackground.draw(batch, 284f, BOTTOM_LINE, 40f, 26f);
        if (ammo != null) {
            Sprite ammoSprite;
            ammoSprite = atlas.getItem(ammo.category);
            ammoSprite.setPosition(285f, 8f);
            ammoSprite.draw(batch);
            textFont.setColor(Color.WHITE);
            textFont.draw(batch, " x" + ammo.amount, 291f, 22f);
        }

        //Floor
        int offsetX = isPadAtRight ? 0 : MyLittleRogueLike.SCREENWIDTH - 55;
        listBackground.draw(batch, 2 + offsetX, 34, 51f, 26f);
        atlas.getTerrain(LevelType.CRYPT).render(batch, 6 + offsetX, 40, Tile.STAIRCASEDOWN, WallJoint.NONE, 0, 1, 0);
        textFont.setColor(Color.WHITE);
        textFont.draw(batch, (floor + 1) + "F", 25 + offsetX, 54, 25f, Align.center, false);

        if (showPad) {
            currentPad = isPadAtRight ? 0 : 1;
            pads[currentPad].render(batch, isAiming);
        }

        if (isDead) {
            drawDeadPanel();
        }
    }

    private void drawDeadPanel() {
        if (useGraphics) {
            deadPanel.draw(batch);
        } else {
            asciiPanel.draw(batch, deadPanel.getRect(), "You have been slain!");
        }
    }

    public boolean buttonInventoryPressed(Vector2 position) {
        return buttonInventory.pressed(position);
    }

    public boolean buttonSkipPressed(Vector2 position, boolean aiming) {
        return buttonSkip.pressed(position) || (!aiming && pads[currentPad].isCenterPressed(position));
    }

    public boolean buttonAimPressed(Vector2 position) {
        return buttonAim.pressed(position) || pads[currentPad].isAimPressed(position);
    }

    public boolean buttonConfigPressed(Vector2 position) {
        return buttonConfig.pressed(position);
    }

    public boolean buttonShootPressed(Vector2 position) {
        return pads[currentPad].isCenterPressed(position);
    }

    @Override
    public void dispose() {
        interfaceAtlas.dispose();
    }

    public Direction padPressed(Vector2 touchPoint) {
        return pads[currentPad].pressedDirection(touchPoint);
    }

    public boolean resetdPad() {
        return pads[currentPad].reset();
    }

    public boolean deadPanelPressed(Vector2 touchPoint) {
        return deadPanel.pressed(touchPoint);
    }

    public void switchAtlas() {
        useGraphics = !useGraphics;
    }
}
