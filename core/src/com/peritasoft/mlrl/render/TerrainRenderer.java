/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.peritasoft.mlrl.dungeongen.Tile;
import com.peritasoft.mlrl.dungeongen.WallJoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class TerrainRenderer {
    private static final int VARIATIONS = 6;
    private Map<Tile, TileRenderer[]> tiles;

    TerrainRenderer(TextureAtlas atlas, String type, String ground) {
        tiles = new HashMap<>();
        tiles.put(Tile.GROUND, groundVariations(atlas, ground));
        tiles.put(Tile.LAVA, getLiquidTerrain(atlas, "lava"));
        tiles.put(Tile.WALL, wallVariations(atlas, type));
        tiles.put(Tile.STAIRCASEDOWN, new TileRenderer[]{
                new SpriteTileRenderer(TextureAtlasHelper.mustCreateSprite(atlas, type + "stairs_down"))
        });
        tiles.put(Tile.STAIRCASEUP, new TileRenderer[]{
                new SpriteTileRenderer(TextureAtlasHelper.mustCreateSprite(atlas, type + "stairs_up"))
        });
    }

    private TileRenderer[] getLiquidTerrain(TextureAtlas atlas, String kind) {
        TileRenderer[] variations = new TileRenderer[2];
        variations[0] = new SpriteTileRenderer(TextureAtlasHelper.mustCreateSprite(atlas, kind,1));
        Sprite[] sprites = new Sprite[]{
                TextureAtlasHelper.mustCreateSprite(atlas, kind, 2),
                TextureAtlasHelper.mustCreateSprite(atlas, kind, 3)
        };
        variations[1] = new AnimatedTileRenderer(new Animation<>(0.5f, sprites));
        return variations;
    }

    private static TileRenderer[] wallVariations(TextureAtlas atlas, String type) {
        TileRenderer[] walls = new TileRenderer[VARIATIONS * WallJoint.values().length];
        for (WallJoint join : WallJoint.values()) {
            System.arraycopy(
                    getVariations(atlas, type + "wall_" + join.toString(), VARIATIONS),
                    0, walls,
                    VARIATIONS * join.toInt(), VARIATIONS
            );
        }
        return walls;
    }

    private static TileRenderer[] groundVariations(TextureAtlas atlas, String ground) {
        ArrayList<TileRenderer> groundVar = new ArrayList<>(Arrays.asList(getVariations(atlas, "floor_" + ground,VARIATIONS)));
        groundVar.addAll(Arrays.asList(getVariations(atlas, "carpet", 5)));
        return groundVar.toArray(new TileRenderer[0]);

    }

    private static TileRenderer[] getVariations(TextureAtlas atlas, String kind, int variations) {
        TileRenderer[] regions = new TileRenderer[variations];
        for (int v = 0; v < variations; v++) {
            regions[v] = new SpriteTileRenderer(TextureAtlasHelper.mustCreateSprite(atlas, kind, v + 1));
        }
        return regions;
    }

    void render(SpriteBatch batch, int x, int y, Tile tile, WallJoint wallJoint, int variation, float alpha, float timer) {
        if (tile == Tile.STAIRCASEUP) {
            render(batch, x, y, Tile.GROUND, WallJoint.NONE, 0, alpha, timer);
        }
        TileRenderer[] tileRenderers = tiles.get(tile);
        if (tileRenderers == null) return;
        TileRenderer tileRenderer = tileRenderers[(wallJoint.toInt() * VARIATIONS) + variation % tileRenderers.length];
        tileRenderer.render(batch, x, y, alpha, timer);
    }
}
