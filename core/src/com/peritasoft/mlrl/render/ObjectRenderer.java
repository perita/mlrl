/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.render;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

class ObjectRenderer {
    Sprite[] regions;

    ObjectRenderer(TextureAtlas atlas, String kind, int variations) {
        regions = new Sprite[variations];
        for (int v = 0; v < variations; v++) {
            regions[v] = TextureAtlasHelper.mustCreateSprite(atlas, kind, v + 1);
        }
    }

    void render(SpriteBatch batch, int x, int y, int variation, float alpha) {
        Sprite sprite = regions[variation % regions.length];
        sprite.setPosition(x, y);
        sprite.draw(batch, alpha);
    }
}
