/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.effects;

import com.badlogic.gdx.Gdx;
import com.peritasoft.mlrl.dungeongen.Position;

public abstract class LifeObj {
    private float lifeTime;
    private Position position;
    private LifeObjType type;
    private final LifeObjLayer layer;

    public LifeObj(Position position, LifeObjLayer layer, LifeObjType type) {
        this.position = position;
        this.lifeTime = 0;
        this.layer = layer;
        this.type = type;
    }

    public void update(boolean nextTurn) {
        lifeTime += Gdx.graphics.getDeltaTime();
    }

    public abstract boolean isAlive();

    public float getLifeTime() {
        return lifeTime;
    }

    public LifeObjType getType() {
        return type;
    }

    public LifeObjLayer getLayer() {
        return layer;
    }

    public Position getPosition() {
        return position;
    }

    public int getPositionX() {
        return position.getX();
    }

    public int getPositionY() {
        return position.getY();
    }
}
