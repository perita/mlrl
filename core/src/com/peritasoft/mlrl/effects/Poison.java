/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.effects;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Cell;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;
import com.peritasoft.mlrl.events.GameEvent;

public class Poison extends LifeTurnObj {
    private final int offsetX;
    private final int offsetY;
    private final int dmg;
    private final Level level;
    private final Character attacker;

    public Poison(int dmg, Level level, Character attacker, int offsetX, int offsetY) {
        // Use the reference to the attacker’s position, *not* a copy, so that this
        // object’s position will be “updated” every time the character moves.
        super(attacker.getPosition(), 4, LifeObjLayer.UNDER, LifeObjType.POISON);
        this.dmg = dmg;
        this.level = level;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.attacker = attacker;
    }

    public Poison(int dmg, Level level, Position position, Character attacker, int turns) {
        // Use the reference to the attacker’s position, *not* a copy, so that this
        // object’s position will be “updated” every time the character moves.
        super(position, turns, LifeObjLayer.UNDER, LifeObjType.POISON);
        this.dmg = dmg;
        this.level = level;
        this.offsetX = 0;
        this.offsetY = 0;
        this.attacker = attacker;
    }

    @Override
    public void update(boolean nextTurn) {
        super.update(nextTurn);
        if (nextTurn && isAlive()) {
            Cell cell = level.getCell(getPositionX(), getPositionY());
            if (cell.hasCharacter()) {
                final Character e = cell.getCharacter();
                e.receiveHit(e == attacker ? dmg/2 : dmg, attacker);
                GameEvent.attackByPoison(attacker, e, dmg);
            }
        }
    }

    @Override
    public int getPositionX() {
        return super.getPositionX() + offsetX;
    }

    @Override
    public int getPositionY() {
        return super.getPositionY() + offsetY;
    }
}
