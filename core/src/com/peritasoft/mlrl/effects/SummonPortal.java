/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.effects;

import com.peritasoft.mlrl.characters.Character;
import com.peritasoft.mlrl.dungeongen.Level;
import com.peritasoft.mlrl.dungeongen.Position;

public class SummonPortal extends LifeTimeObj {
    private final Level level;
    private Character enemySummoned;

    public SummonPortal(Level level, Character enemySummoned) {
        super(new Position(enemySummoned.getPosition()), 0.5f, LifeObjLayer.UNDER, LifeObjType.MAGIC_CIRCLE);
        this.level = level;
        this.enemySummoned = enemySummoned;
    }

    @Override
    public void update(boolean nextTurn) {
        super.update(nextTurn);
        if (getLifeTime() > getTimeToLive() / 2 && enemySummoned != null
                && !level.hasCharacterIn(enemySummoned.getPosition())
                && level.isWalkable(enemySummoned.getPosition())) {
            level.addEnemy(enemySummoned);
            enemySummoned = null;
        }
    }
}
