/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.PlayerHero;

public class CaveLevelGenerator implements LevelGenerator {

    private final int width;
    private final int height;

    public CaveLevelGenerator(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        Tile[][] tiles = new Tile[height][width];
        randomizeTiles(tiles);
        smooth(tiles, 7);
        removeSingleWalls(tiles);
        Position startingPosition = LevelUtils.putStaircases(tiles, upStaircase, true);
        return new Level(tiles, startingPosition, LevelType.CAVE);
    }

    private void randomizeTiles(Tile[][] tiles) {
        final int height = tiles.length;
        final int width = tiles[0].length;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (y == 0 || y == height - 1 || x == 0 || x == width - 1) {
                    tiles[y][x] = Tile.WALL;
                } else {
                    tiles[y][x] = MathUtils.randomBoolean(0.55f) ? Tile.GROUND : Tile.WALL;
                }
            }
        }
    }

    private void smooth(Tile[][] tiles, final int times) {
        final int height = tiles.length;
        final int width = tiles[0].length;
        Tile[][] tiles2 = new Tile[height][width];
        for (int time = 0; time < times; time++) {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    if (y == 0 || y == height - 1 || x == 0 || x == width - 1) {
                        tiles2[y][x] = Tile.WALL;
                    } else {
                        int[] wallsFloors;
                        int[] wallsFloors5;
                        if (time < times - 3) {
                            //recorregut de 3x3
                            wallsFloors = LevelUtils.countSurroundingWallsFloors(tiles, x, y, 1);
                            //recorregut de 5x5
                            wallsFloors5 = LevelUtils.countSurroundingWallsFloors(tiles, x, y, 2);

                            if (wallsFloors[0] > wallsFloors[1] || wallsFloors5[0] <= 2) {
                                tiles2[y][x] = Tile.WALL;
                            } else {
                                tiles2[y][x] = Tile.GROUND;
                            }
                        } else {
                            //recorregut de 3x3
                            wallsFloors = LevelUtils.countSurroundingWallsFloors(tiles, x, y, 1);
                            tiles2[y][x] = wallsFloors[1] >= wallsFloors[0] ? Tile.GROUND : Tile.WALL;
                        }
                    }
                }
            }
            for (int y = 1; y < height - 1; y++) {
                System.arraycopy(tiles2[y], 1, tiles[y], 1, width - 2);
            }
        }
    }

    private void removeSingleWalls(Tile[][] tiles) {
        final int height = tiles.length;
        final int width = tiles[0].length;
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                if (tiles[y][x] == Tile.WALL && LevelUtils.countSurroundingWallsFloors(tiles, x, y, 1)[0] == 1) {
                    tiles[y][x] = Tile.GROUND;
                }
            }
        }
    }

}
