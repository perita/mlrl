/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.mlrl.characters.Ivrexa;
import com.peritasoft.mlrl.characters.PlayerHero;
import com.peritasoft.mlrl.effects.LifeTurnObj;
import com.peritasoft.mlrl.effects.Rests;
import com.peritasoft.mlrl.props.Coins;
import com.peritasoft.mlrl.props.LightPoint;
import com.peritasoft.mlrl.props.PropType;

import java.util.Arrays;

public class IvrexaRoomLevelGenerator implements LevelGenerator {
    private static final int ROOM_WIDTH = 20;
    private static final int ROOM_HEIGHT = 12;

    @Override
    public Level generate(int floor, boolean upStaircase, PlayerHero player) {
        //Room width/Heith is constant
        Tile[][] tiles = new Tile[ROOM_HEIGHT][ROOM_WIDTH];
        fillWithGround(tiles);
        putWalls(tiles);
        Position startingPosition = new Position(5, 6);
        String[] logMessages = new String[]{
                "Welcome to my lair you fool",
                "Prepare to burn on suffering"
        };
        final Level level = new BossLevel(tiles, startingPosition, LevelType.CAVE, logMessages);
        generateBoss(level, floor, player.getLevel());
        decorateRoom(level);
        return level;
    }

    private void fillWithGround(Tile[][] tiles) {
        for (Tile[] row : tiles) {
            Arrays.fill(row, Tile.GROUND);
        }
    }

    private void putWalls(Tile[][] tiles) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[0].length; j++) {
                if (i == 0 || i == tiles.length - 1 || j == 0 || j == tiles[0].length - 1) {
                    tiles[i][j] = Tile.WALL;
                }
                //sharpened borders
                if (i == 1 || j == 1 || i == tiles.length - 2 || j == tiles[0].length - 2) {
                    if (MathUtils.random() < 0.60f) {
                        tiles[i][j] = Tile.WALL;
                    }
                }
            }
        }
        smooth(tiles, 2);

        //center lava lake
        tiles[6][12] = Tile.LAVA;
        tiles[6][11] = Tile.LAVA;
        tiles[6][10] = Tile.LAVA;
        tiles[6][9] = Tile.LAVA;
        tiles[7][10] = Tile.LAVA;
        tiles[7][9] = Tile.LAVA;
        tiles[7][8] = Tile.LAVA;
        tiles[5][9] = Tile.LAVA;
        tiles[5][10] = Tile.LAVA;
        tiles[5][11] = Tile.LAVA;
        tiles[5][12] = Tile.LAVA;
        tiles[4][10] = Tile.LAVA;
    }

    private void smooth(Tile[][] tiles, final int times) {
        final int height = tiles.length;
        final int width = tiles[0].length;
        Tile[][] tiles2 = new Tile[height][width];
        for (int time = 0; time < times; time++) {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    if (y == 0 || y == height - 1 || x == 0 || x == width - 1) {
                        tiles2[y][x] = Tile.WALL;
                    } else {
                        int[] wallsFloors;
                        int[] wallsFloors5;
                        if (time < times - 3) {
                            //recorregut de 3x3
                            wallsFloors = LevelUtils.countSurroundingWallsFloors(tiles, x, y, 1);
                            //recorregut de 5x5
                            wallsFloors5 = LevelUtils.countSurroundingWallsFloors(tiles, x, y, 2);

                            if (wallsFloors[0] > wallsFloors[1] || wallsFloors5[0] <= 2) {
                                tiles2[y][x] = Tile.WALL;
                            } else {
                                tiles2[y][x] = Tile.GROUND;
                            }
                        } else {
                            //recorregut de 3x3
                            wallsFloors = LevelUtils.countSurroundingWallsFloors(tiles, x, y, 1);
                            tiles2[y][x] = wallsFloors[1] >= wallsFloors[0] ? Tile.GROUND : Tile.WALL;
                        }
                    }
                }
            }
            for (int y = 1; y < height - 1; y++) {
                System.arraycopy(tiles2[y], 1, tiles[y], 1, width - 2);
            }
        }
    }

    private void generateBoss(Level level, int floor, int playerLevel) {
        int enemyLevel = Math.max(floor, playerLevel);
        level.addEnemy(new Ivrexa(enemyLevel, new Position(13,6)));
    }

    private void decorateRoom(Level level) {
        level.getCell(4, 10).setProp(new LightPoint());
        level.getCell(8, 10).setProp(new LightPoint());
        level.getCell(12, 10).setProp(new LightPoint());
        level.getCell(16, 10).setProp(new LightPoint());
        level.getCell(4, 2).setProp(new LightPoint());
        level.getCell(8, 2).setProp(new LightPoint());
        level.getCell(12, 2).setProp(new LightPoint());
        level.getCell(16, 2).setProp(new LightPoint());
        level.getCell(2, 6).setProp(new LightPoint());
        level.getCell(18, 6).setProp(new LightPoint());
        level.getCell(17, 8).setProp(new Coins(PropType.COINS_SILVER));
        level.getCell(17, 7).setProp(new Coins(PropType.COINS_GOLD));
        level.getCell(17, 6).setProp(new Coins(PropType.COINS_SILVER));
        level.getCell(16, 7).setProp(new Coins(PropType.COINS_SILVER));
        level.getCell(16, 6).setProp(new Coins(PropType.COINS_COPPER));

        level.add(new Rests(new Position(4,3), LifeTurnObj.INFINITE));
        level.add(new Rests(new Position(5,3), LifeTurnObj.INFINITE));
        level.add(new Rests(new Position(5,4), LifeTurnObj.INFINITE));
        level.add(new Rests(new Position(6,7), LifeTurnObj.INFINITE));
        level.add(new Rests(new Position(6,8), LifeTurnObj.INFINITE));
        level.add(new Rests(new Position(7,8), LifeTurnObj.INFINITE));
        level.add(new Rests(new Position(11,4), LifeTurnObj.INFINITE));
        level.add(new Rests(new Position(12,4), LifeTurnObj.INFINITE));
    }

}
