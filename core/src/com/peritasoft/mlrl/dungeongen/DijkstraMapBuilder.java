/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import java.util.Arrays;

public class DijkstraMapBuilder {
    private int[] next = new int[30 * 30 * 4];
    private boolean[][] visited = new boolean[30][30];
    private int writeIndex = 0;
    private Level level;
    private DijkstraMap map;

    public void reset(Level level, DijkstraMap map) {
        this.level = level;
        this.map = map;

        final int width = level.getWidth();
        final int height = level.getHeight();
        map.reset(width, height);
        if (visited.length != height || visited[0].length != width) {
            visited = new boolean[height][width];
            next = new int[height * width * 4];
        }
        for (int row = 0; row < height; row++) {
            Arrays.fill(visited[row], false);
        }
        writeIndex = 0;
    }

    public void addGoal(int row, int col) {
        if (isWalkable(row, col)) {
            map.setDistance(row, col, 0f);
            enqueueNext(row, col);
        }
    }

    public void build() {
        final int width = level.getWidth();
        for (int readIndex = 0; readIndex < writeIndex; readIndex++) {
            int row = next[readIndex] / width;
            int col = next[readIndex] % width;
            for (Direction dir : Direction.values()) {
                enqueueNext(row + dir.y, col + dir.x);
            }
            computeDistanceAround(row, col);
        }
    }

    private void enqueueNext(int row, int col) {
        if (!isWalkable(row, col) || visited[row][col]) {
            return;
        }
        next[writeIndex] = row * level.getWidth() + col;
        visited[row][col] = true;
        writeIndex++;
    }

    private void computeDistanceAround(int row, int col) {
        float min = Float.MAX_VALUE;
        for (Direction dir : Direction.values()) {
            int neighborRow = row + dir.y;
            int neighborCol = col + dir.x;
            float distance = isWalkable(neighborRow, neighborCol)
                    ? map.getDistance(neighborRow, neighborCol)
                    : Float.MAX_VALUE;
            min = Math.min(min, distance);
        }
        map.setDistance(row, col, Math.min(map.getDistance(row, col), min + 1f));
    }

    private boolean isWalkable(int row, int col) {
        return level.isWalkable(col, row);
    }

}
