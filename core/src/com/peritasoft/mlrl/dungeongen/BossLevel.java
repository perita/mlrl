/*
 * My Little Roguelike
 * Copyright (C) 2019-2020 David Pérez
 * Copyright (C) 2019-2020 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.peritasoft.mlrl.dungeongen;

import com.badlogic.gdx.graphics.Color;
import com.peritasoft.mlrl.events.Log;

public class BossLevel extends Level {
    private final String[] logMessages;

    public BossLevel(Tile[][] tiles, Position startingPosition, LevelType type, String[] logMessages) {
        super(tiles, startingPosition, type);
        setAllVisible();
        this.logMessages = logMessages;
    }

    private void setAllVisible() {
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                this.getCell(i,j).setVisibility(1f);
            }
        }
    }

    public String[] getLogMessages() {
        return logMessages;
    }

    @Override
    public boolean hasFov() {
        return false;
    }

    @Override
    public boolean canAscendToPreviousFloor(Position position) {
        return false;
    }

    @Override
    public void logEnter(Log log) {
        if (hasEnemies()) {
            for (String msg : logMessages) {
                log.add(msg, Color.RED);
            }
        } else {
            log.add("This Room is fulfilled with darkness and a weird smell, but seems empty", Color.RED);
        }
    }
}
